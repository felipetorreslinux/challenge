import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
            'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
            sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        z-index:0 ;
        user-select: none !important;
    }

    .leaflet-container{
        z-index:0 !important;
    }

    .leaflet-container .leaflet-control{
        display:none !important ;
    }

    .page-root{
        display:flex ;
        flex-direction:column ;
        height:100vh ;
        overflow:hidden ;
        z-index:0 ;
    }
`