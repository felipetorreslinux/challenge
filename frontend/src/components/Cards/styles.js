import styled from 'styled-components/native';

export const Container = styled.FlatList`
    position:absolute !important ;
    bottom:0 ;
    width:calc(100vw - 2rem) ;
    margin:1rem ;
    z-index:1 ;
`;

export const Card = styled.View`
    display:flex ;
    flex-direction:column ;
    background-color:#fff ;
    padding:1rem ;
    width:300px ;
    border-radius:5px ;
    margin-right:1rem ;
    
    @media(max-width:480px) {
        width:300px;
    }
`

export const Name = styled.Text`
    font-size:15px ;
    color: #000 ;
    font-weight:bold ;
    text-transform:uppercase ;
`
export const Cnpj = styled.Text`
    font-size:13px ;
    color: #707070 ;
`

export const LatLng = styled.View`
    display:flex ;
    flex-direction:row ;
    align-items:center ;
    justify-content:space-evenly ;
    margin-top:0.5rem ;
`

export const Text = styled.Text`
    font-size:13px ;
    line-height:14px ;
    color: #303030 ;
`