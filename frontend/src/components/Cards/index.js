import React, { useEffect } from 'react';
import { useClinic } from '../../contexts/clinic';
import { handleMaskCnpj } from '../../utils';

import { Container, Card, Name, Cnpj, LatLng, Text } from './styles';

function Cards() {
    
    const { clinics, handleListAllClinics } = useClinic();
    
    useEffect(() => handleListAllClinics(), []);

    return (
        <Container
        data={clinics}
        horizontal
        overScrollMode="never"
        nestedScrollEnabled={false}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
            <Card>
                <Name>{item.name}</Name>
                <Cnpj>{handleMaskCnpj(item.cnpj)}</Cnpj>
                <LatLng>
                    <Text>Lat: {item.latitude.toFixed(6)}</Text>
                    <Text>Lng: {item.longitude.toFixed(6)}</Text>
                </LatLng>
            </Card>
        )} />
    )
}

export default Cards;