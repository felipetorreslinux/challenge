import React, { useState } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import { 
    Container,
    Place,
    Places,
    Title,
    Wrapper
} from './styles';

import { handleExtractorAddress, handleIsCnpj } from '../../utils';

import { useMaps } from '../../contexts/maps';
import { useClinic } from '../../contexts/clinic';

function Form() {

    const { handleStoreClinic } = useClinic();
    const { markers, setMarkers, handleGetPlaces } = useMaps();

    const [name, setName] = useState("");
    const [cnpj, setCnpj] = useState("");
    const [address, setAddress] = useState("");

    const [place, setPlace] = useState("");
    const [places, setPlaces] = useState([]);

    const [alert, setAlert] = useState({ visible:false, type:null, message: null });

    const handleClearForm = () => {
        setName("");
        setCnpj("");
        setAddress("");
        setPlace("");
    }

    const handleResultsPlace = async () => {
        if(place && place.length > 3) {
           const response = await handleGetPlaces(place);
           return setPlaces(response);
        }
        return setPlace([]);
    }

    const handlePlaceSelected = (address) => {    
        if(address){
            const findMarker = markers.findIndex(marker => marker === address);
            if(findMarker < 0){
                const { lat, lng } = address.geometry.location;
                setMarkers([...markers, { lat, lng, place:address.formatted_address }]);
                setAddress(address)
                setPlace(address.formatted_address);
                setPlaces([]);
            }
        }
    }

    const saveClinic = async () => {
        if(!name){
            setAlert({visible:true, type:'error', message:"Informe o Nome"});
        }else if(!cnpj){
            setAlert({visible:true, type:'error', message:"Informe o CNPJ"});
        }else if(!handleIsCnpj(cnpj)){
            setAlert({visible:true, type:'error', message:"CNPJ Inválido"});
        }else if(!address){
            setAlert({visible:true, type:'error', message:"Informe o Endereço"});
        }else{
            try{
                const response = await handleStoreClinic({
                    name, 
                    cnpj,
                    place:handleExtractorAddress(address,'route'),
                    number:handleExtractorAddress(address,'street_number'),
                    district:handleExtractorAddress(address,'sublocality'),
                    complement:handleExtractorAddress(address,'postal_code'),
                    city:handleExtractorAddress(address,'administrative_area_level_2'),
                    state:handleExtractorAddress(address,'administrative_area_level_1'),
                    country:handleExtractorAddress(address,'country'),
                    latitude:address.geometry.location.lat,
                    longitude:address.geometry.location.lng
                });                
                if(response.success){
                    handleClearForm();
                }else{
                    setAlert({visible:true, type:'error', message:response.message});
                }
            }catch(e){
                setAlert({visible:true, type:'error', message:e.message});
            }
            
        }
    }

    return (
        <Container>
            <Title>Cadastro de Clínicas</Title>
            <Wrapper>
                {
                    alert.visible &&
                    <Alert 
                    style={{marginBottom:"1rem"}} 
                    severity={alert.type}>
                        {alert.message}
                    </Alert>
                }
                <Grid container spacing={2}>
                    <Grid item md={12} xs={12}>
                        <TextField
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        inputProps={{max:50, maxLength:50}}
                        label="NOME DA CLÍNICA"
                        InputLabelProps={{shrink:true}}
                        fullWidth />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                        value={cnpj}
                        onChange={(e) => setCnpj(e.target.value.replace(/\D/g,''))}
                        inputProps={{max:14, maxLength:14}}
                        label="CNPJ"
                        InputLabelProps={{shrink:true}}
                        fullWidth />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <TextField
                        value={place}
                        onChange={(e) => setPlace(e.target.value)}
                        inputProps={{max:100, maxLength:100}}
                        label="ENDEREÇO"
                        InputLabelProps={{shrink:true}}
                        InputProps={{
                            endAdornment:(
                                <>
                                    {
                                        (place && place.length > 3) &&
                                        <Button
                                        style={{marginLeft:'1rem'}}
                                        size="small"
                                        variant="text"
                                        onClick={handleResultsPlace}>
                                            Buscar
                                        </Button>
                                    }
                                </>
                            )
                        }}
                        fullWidth />
                        {
                            places.length > 0 &&
                            <Places>
                                {
                                    places.map(place => (
                                        <Place 
                                        key={place.place_id}
                                        onClick={() =>handlePlaceSelected(place)}>
                                            {place.formatted_address}
                                        </Place>
                                    ))
                                }
                            </Places>
                        }
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <TextField
                        value={address && address.geometry.location.lat.toFixed(6)}
                        label="LATITUDE"
                        InputLabelProps={{shrink:true}}
                        disabled
                        fullWidth />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <TextField
                        value={address && address.geometry.location.lng.toFixed(6)}
                        label="LONGITUDE"
                        InputLabelProps={{shrink:true}}
                        disabled
                        fullWidth />
                    </Grid>
                    <Grid item md={12} xs={12}>
                        <Button                        
                        size="small"
                        variant="outlined"
                        fullWidth
                        onClick={saveClinic}>
                            Cadastrar
                        </Button>
                    </Grid>
                </Grid>  
            </Wrapper>
        </Container>
    )
}

export default Form;