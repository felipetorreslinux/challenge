import styled from 'styled-components';

export const Container = styled.div`
    position:absolute ;
    display:flex ;
    flex-direction:column ;
    padding:1rem ;
    background-color:#fff ;
    width:300px ;
    z-index:1 ;
    margin: 1rem ;
    border-radius:5px ;
    box-shadow:0px 2px 10px #c3c3c3 ;

    @media(max-width: 480px){
        display:none !important ;
    }
`;

export const Title = styled.span`
    font-size:14px ;
    font-weight:bold ;
    color:#707070 ;
    text-align:center ;
    text-transform:uppercase ;
`

export const Wrapper = styled.div`
    display:flex ;
    flex-direction:column ;
    margin-top:1rem ;
`


export const Places = styled.div`
    display:flex ;
    flex-direction:column ;
    padding:1rem 0rem 0rem ;
`

export const Place = styled.span`
    font-size:16px ;
    color:#000;    
    background-color:#f7f7f7 ;
    padding:0.5rem;
    margin-top: ${props => props.index !== 0 ? '0.5rem' : '0rem'} ;
    cursor:pointer ;
`