import React from 'react';
import { Typography } from '@material-ui/core';
import { MapContainer, Marker, Popup, TileLayer, useMap } from 'react-leaflet';
import { useClinic } from '../../contexts/clinic';
import { useMaps } from '../../contexts/maps';

function Markers () {
  const map = useMap();  
  const { markers } = useMaps();
  if(markers.length > 0){
    const startMarker = [markers[0].lat,markers[0].lng]
    const endMarker = [markers[markers.length - 1].lat, markers[markers.length - 1].lng]
    map.fitBounds([startMarker, endMarker], {maxZoom:5});
    return markers.map( marker => (
      <Marker
      key={marker.place}
      position={marker}>
        <Popup>
          <Typography variant="subtitle2" align='center'>
            {marker.place}
          </Typography>
        </Popup>
      </Marker>
    ));
  }
}

function Clinics () {
  const map = useMap();  
  const { clinics } = useClinic();
  if(clinics.length > 0){
    const startClinic = [clinics[0].latitude,clinics[0].longitude]
    const endClinic = [clinics[clinics.length - 1].latitude, clinics[clinics.length - 1].longitude]
    map.fitBounds([startClinic, endClinic], {maxZoom:5});
    return clinics.map( clinic => (
      <Marker 
      key={clinic.id}
      position={{lat:clinic.latitude, lng:clinic.longitude}}>
        <Popup>
          <Typography variant="subtitle2" align='center'>
            {clinic.place}, {clinic.number} - {clinic.district} - {clinic.city} - {clinic.state}, {clinic.complement} - {clinic.country}
          </Typography>
        </Popup>
      </Marker>
    ))
  }
}

function Maps() {

  return (
    <MapContainer
      style={{width:"100%", height:"100%"}}
      center={[-8.2350, -35.9253]}
      minZoom={4}
      zoom={4}>
      <Markers />
      <Clinics />
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
    </MapContainer>
  )
}

export default Maps;