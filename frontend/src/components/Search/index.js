import React, { useRef, useState } from 'react';
import { Button, TextField } from '@material-ui/core';
import { Container, Place, Results } from './styles';
import { useMaps } from '../../contexts/maps';

function Search() {

    const { markers, setMarkers, handleGetPlaces } = useMaps();

    const ref_place = useRef();

    const [place, setPlace] = useState("");
    const [places, setPlaces] = useState([]);

    const handleResultsPlace = async () => {
        if(place && place.length > 3) {
          const response = await handleGetPlaces(place);
          setPlaces(response);
        }else{
            ref_place.current.focus();
        }
    }

    const handlePlaceSelected = (address) => {
        if(address){
            const findMarker = markers.findIndex(marker => marker === address);
            if(findMarker < 0){
                const { lat, lng } = address.geometry.location;
                setMarkers([...markers, {lat, lng, place:address.formatted_address}]);
            }
            setPlace("");
            setPlaces([]);
        }
    }

    return (
        <Container>
            <TextField
            inputRef={ref_place}
            value={place}
            onChange={(e) => setPlace(e.target.value)}
            inputProps={{max:100, maxLength:100}}
            placeholder='Endereço'
            InputProps={{
                endAdornment:(
                    <Button
                    size="small"
                    variant="text"
                    onClick={handleResultsPlace}>
                        Buscar
                    </Button>
                )
            }}
            fullWidth />

            {
                places.length > 0 &&
                <Results>
                    {
                        places.map((address, index) => (
                            <Place 
                            {...index}
                            key={place.place_id}
                            onClick={()=> handlePlaceSelected(address)}>
                                {address.formatted_address}
                            </Place>
                        ))
                    }
                </Results>
            }

        </Container>
    )
}

export default Search;