import styled from 'styled-components';

export const Container = styled.div`
    position:absolute ;
    right:0 ;
    display:flex ;
    flex-direction:column ;
    padding:1rem ;
    background-color:#fff ;
    width:calc(100% - (300px + 7rem)) ;
    margin:1rem;
    border-radius:5px ;
    box-shadow:0px 2px 10px #c3c3c3 ;
    z-index:1 ;

    @media(max-width: 480px){
        width:calc(100% - 4rem);
    }
`;

export const Results = styled.div`
    display:flex ;
    flex-direction:column ;
    padding:1rem 0rem 0rem ;
`

export const Place = styled.span`
    font-size:16px ;
    color:#000;    
    background-color:#f7f7f7 ;
    padding:0.5rem;
    margin-top: ${props => props.index !== 0 ? '0.5rem' : '0rem'} ;
    cursor:pointer ;
`
