import { ClinicProvider } from './contexts/clinic';
import { MapsProvider } from './contexts/maps';

import React from 'react';
import Cards from './components/Cards';
import Form from './components/Form';
import Maps from './components/Maps';
import Search from './components/Search';

function App() {

  return (
    <ClinicProvider>
      <MapsProvider>
        <div className='page-root'>
          <Cards />
          <Form />
          <Search />
          <Maps />
        </div>
      </MapsProvider>
    </ClinicProvider>
  )
}

export default App;