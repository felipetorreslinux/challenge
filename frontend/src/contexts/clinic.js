import { createContext, useContext, useState } from 'react';
import { getClinics, storeClinic } from '../services/clinics';

const ClinicContext = createContext({});

export const ClinicProvider = ({ children }) => {

    const [clinics, setClinics] = useState([]);

    const handleListAllClinics = async () => {
        const response = await getClinics();
        if(response.success) setClinics(response.clinics);
    }

    const handleStoreClinic = async (clinic) => {
        const response = await storeClinic(clinic);
        if(response.success){
            handleListAllClinics();
        }
        return response;
    };

    return (
        <ClinicContext.Provider
        value={{ clinics, handleListAllClinics, handleStoreClinic}}>
            {children}
        </ClinicContext.Provider>
    )
}

export const useClinic = () => useContext(ClinicContext);