import { createContext, useContext, useState } from 'react';
import { getAddress } from '../services/maps';

const MapsContext = createContext({});

export const MapsProvider = ({ children }) => {
    
    const [markers, setMarkers] = useState([]);    
    
    const handleGetPlaces = async (address) => {
        const response = await getAddress(address);
        if(response.status === "OK") return response.results;
        return [];
    };

    return (
        <MapsContext.Provider 
        value={{ markers, setMarkers, handleGetPlaces }}>
            {children}
        </MapsContext.Provider>
    )
}

export const useMaps = () => useContext(MapsContext);