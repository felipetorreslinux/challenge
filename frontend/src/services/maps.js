import axios from "axios";

export const getAddress = async (address) => {
    const request = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyAEoROAZmwSkWHZnqoBrbxUAxJ9SbJ2tfc`);
    return await request.data;
}

