import { api } from "./api";

export const getClinics = async () => {
    const request = await api.get('/clinics');
    return await request.data;
}

export const storeClinic = async (clinic) => {
    const request = await api.post('/clinics', { clinic });
    return await request.data;
}