import { Request, Response } from 'express';
import { all, store, findCnpj } from '../models/Clinic';

export default {
    
    async index (req: Request, res: Response) {
        try{
            const clinics = await all();
            return res.status(200).json({ success:true, clinics });
        }catch(e:any){
            return res.status(400).json({ success:false, message:e.message });
        }
    },

    async store (req: Request, res: Response) {
        try{
            const { clinic } = req.body;
            const isClinic = await findCnpj(clinic.cnpj);        
            if(isClinic) throw new Error('Clinic already registered');
            await store(clinic);
            return res.status(201).json({ success:true, message:"Successfully Registered" });
        }catch(e:any){
            return res.status(400).json({ success:false, message:e.message });
        }
    }
    
}