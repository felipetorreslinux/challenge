export interface IClinic {
    id?: string;
    name: string;
    cnpj: string;
    place: string;
    number: string;
    district: string;
    complement: string;
    city: string;
    country: string;
    state: string;
    latitude: number;
    longitude: number;
    created_at?: string;
}