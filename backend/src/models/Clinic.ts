import knex from '../database';
import { IClinic } from '../interfaces/Clinic';

export const findCnpj = async (cnpj: string) => {
    return await knex
    .from('clinics')
    .where({ cnpj })
    .first();
}

export const all = async () => {
    return await knex
    .from('clinics')
    .orderBy('name', 'asc');
}

export const store = async (clinic: IClinic) => {
    const { name, cnpj, place, number, district, complement, city, country, state, latitude, longitude } = clinic;
    return await knex.from('clinics')
    .insert({ 
        name, 
        cnpj, 
        place, 
        number, 
        district, 
        complement, 
        city, 
        country, 
        state, 
        latitude, 
        longitude
    });
}