import express from 'express';
import cors from 'cors';
import { routes } from './routes';
const app = express()
app.use(express.json({limit:'2mb'}))
app.use(cors())
app.use(routes);
app.listen(3333);