import { Router } from "express";

import ClinicController from "../controllers/ClinicController";

const clinics = Router();

clinics.get('/clinics', ClinicController.index);
clinics.post('/clinics', ClinicController.store);

export { clinics };