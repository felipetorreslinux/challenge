import { Router } from "express";
import { clinics } from "./clinics";

const routes = Router();

routes.use(clinics);

export { routes };