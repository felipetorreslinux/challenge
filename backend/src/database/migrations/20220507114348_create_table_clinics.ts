import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('clinics', table => {
        
        table.uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'));

        table.string('name', 150).notNullable();
        table.string('cnpj', 14).notNullable();
        
        table.string('place', 150).notNullable();
        table.string('number', 6).notNullable();
        table.string('district', 100).notNullable();
        table.string('complement', 200);
        table.string('city', 100).notNullable();
        table.string('state', 2).notNullable();
        table.string('country', 3).notNullable();
        table.double('latitude', 10).notNullable();
        table.double('longitude', 10).notNullable();

        table.timestamp('created_at').defaultTo(knex.fn.now());

    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('clinics');
}

