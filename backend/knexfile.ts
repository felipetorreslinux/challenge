module.exports = {
  client: 'pg',
  searchPath:['challenge'],
  connection: {
    host:'host',
    database: 'database',
    user:     'user',
    password: 'password'
  },
  migrations: {
    tableName: 'db_challenge',
    directory:'./src/database/migrations'
  }
};
